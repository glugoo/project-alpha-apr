from django.contrib import admin
from tasks.models import Task


@admin.register(Task)
class Project(admin.ModelAdmin):
    list_display = (
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
    )


# Register your models here.
